﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Akka.Actor;
using Chat.ActorSystem.Messages;

namespace Chat.ActorSystem.Actors
{
    public class ChatServerActor : ReceiveActor
    {
        private readonly HashSet<IActorRef> _clients = new HashSet<IActorRef>();

        public ChatServerActor()
        {
            Receive<SayRequest>(message => Say(message));
            Receive<ConnectRequest>(message => Connect(message));
            Receive<NickRequest>(message => NewNickRequest(message));
            Receive<Disconnect>(message => DisconectFromChat(message));
            Receive<ChannelsRequest>(message => RequestChannel(message));
        }

        private void RequestChannel(ChannelsRequest message)
        {
            
        }

        private void DisconectFromChat(Disconnect message)
        {
            
        }

        private void NewNickRequest(NickRequest message)
        {
            var response = new NickResponse(message.OldUsername, message.NewUsername);

            foreach (var client in _clients) client.Tell(response, Self);
        }

        private void Connect(ConnectRequest message)
        {
            //Console.WriteLine("User {0} has connected", message.Username);
            _clients.Add(this.Sender);
            var helloMessage = "Hello and welcome to Akka .NET chat example";
            Sender.Tell(new ConnectResponse(helloMessage), Self);
        }

        private void Say(SayRequest message)
        {
            //Console.WriteLine("User {0} said {1}", message.Username, message.Text);
            var response = new SayResponse(message.Username, message.Text);
            foreach (var client in _clients) client.Tell(response, Self);
        }
    }
}
