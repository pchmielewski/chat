﻿using Akka.Actor;

namespace Chat.ActorSystem.Messages
{
    public class ChannelsResponse
    {
        public IActorRef[] Channels { get; private set; }

        public ChannelsResponse(IActorRef[] channels)
        {
            Channels = channels;
        }
    }
}
