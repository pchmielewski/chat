﻿namespace Chat.ActorSystem.Messages
{
    public class ConnectRequest
    {
        public string Username { get; private set; }

        public ConnectRequest(string username)
        {
            Username = username;
        }
    }
}
