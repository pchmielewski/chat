﻿namespace Chat.ActorSystem.Messages
{
    public class SayRequest
    {
        public string Username { get; private set; }
        public string Text { get; private set; }

        public SayRequest(string username, string text)
        {
            Username = username;
            Text = text;
        }
    }
}
