﻿namespace Chat.ActorSystem.Messages
{
    public class NickRequest
    {
        public string OldUsername { get; private set; }
        public string NewUsername { get; private set; }

        public NickRequest(string oldUsername, string newUsername)
        {
            OldUsername = oldUsername;
            NewUsername = newUsername;
        }
    }
}
