﻿namespace Chat.ActorSystem.Messages
{
    public class SayResponse
    {
        public string Username { get; private set; }
        public string Text { get; private set; }

        public SayResponse(string username, string text)
        {
            Username = username;
            Text = text;
        }
    }
}
