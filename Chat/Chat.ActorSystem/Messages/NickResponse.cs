﻿namespace Chat.ActorSystem.Messages
{
    public class NickResponse
    {
        public string OldUsername { get; private set; }
        public string NewUsername { get; private set; }

        public NickResponse(string oldUsername, string newUsername)
        {
            OldUsername = oldUsername;
            NewUsername = newUsername;
        }
    }
}
