﻿using Akka.Actor;
using Chat.ActorSystem.Actors;

namespace Chat.Server.Models
{
    public static class ChatSystem
    {
        private static Akka.Actor.ActorSystem ActorSystemInstance;

        public static void Create()
        {
            ActorSystemInstance = Akka.Actor.ActorSystem.Create("MyServer");
           ActorSystemInstance.ActorOf<ChatServerActor>("ChatServer");
        }

        public static void Shutdown()
        {
            ActorSystemInstance.Terminate();
            ActorSystemInstance.Dispose();
        }
    }
}
