﻿using System;
using Chat.Server.Models;

namespace Chat.Server
{
    class Program
    {
        static void Main(string[] args)
        {
            ChatSystem.Create();
            Console.ReadLine();
            ChatSystem.Shutdown();
        }
    }
}
