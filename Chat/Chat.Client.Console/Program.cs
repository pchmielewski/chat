﻿using System;
using System.Linq;
using Akka.Actor;
using Chat.ActorSystem.Messages;
using Chat.Client.ConsoleApp.Actors;

namespace Chat.Client.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var system = Akka.Actor.ActorSystem.Create("MyClient"))
            {
                var chatClient = system.ActorOf(Props.Create<ChatClientActor>());
                system.ActorSelection("akka.tcp://MyServer@localhost:8081/user/ChatServer");
                chatClient.Tell(new ConnectRequest(Guid.NewGuid().ToString()));

                while (true)
                {
                    var input = Console.ReadLine();
                    if (input.StartsWith("/"))
                    {
                        var parts = input.Split(' ');
                        var cmd = parts[0].ToLowerInvariant();
                        var rest = string.Join(" ", parts.Skip(1));

                        if (cmd == "/nick")
                        {
                            chatClient.Tell(new NickRequest(null,rest));
                        }
                    }
                    else
                    {
                        chatClient.Tell(new SayRequest(null,input));
                    }
                }
            }
        }
    }
}
