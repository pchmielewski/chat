﻿using System;
using System.Threading.Tasks;
using Akka.Actor;
using Chat.ActorSystem.Messages;

namespace Chat.Client.ConsoleApp.Actors
{
    public class ChatClientActor : ReceiveActor
    {
        private readonly ActorSelection _server = Context.ActorSelection("akka.tcp://MyServer@localhost:8081/user/ChatServer");
        private string _nick = Guid.NewGuid().ToString();

        public ChatClientActor()
        {
            Receive<ConnectRequest>(message => ConnectReq(message));
            Receive<ConnectResponse>(message => Connect(message));
            Receive<NickRequest>(message => ChangeNick(message));
            Receive<NickResponse>(message => ChangeNickResponse(message));
            Receive<SayRequest>(message => SayReq(message));
            Receive<SayResponse>(message => SayRes(message));
            
        }

        private void SayReq(SayRequest message)
        {
            var mes = new SayRequest(_nick, message.Text);
            _server.Tell(mes);
        }

        private void ConnectReq(ConnectRequest message)
        {
            Console.WriteLine("Connecting....");
            _server.Tell(message);
        }

        private void SayRes(SayResponse message)
        {
            Console.WriteLine("{0}: {1}", message.Username, message.Text);
        }

        private void ChangeNickResponse(NickResponse message)
        {
            Console.WriteLine("{0} is now known as {1}", message.OldUsername, message.NewUsername);
        }

        private void ChangeNick(NickRequest message)
        {
            var mes = new NickRequest(_nick, message.NewUsername);
            Console.WriteLine("Changing nick to {0}", mes.NewUsername);
            _nick = mes.NewUsername;
            _server.Tell(mes);
        }

        private void Connect(ConnectResponse message)
        {
            Console.WriteLine("Connected!");
            Console.WriteLine(message.Message);
        }
    }
}
